from st3m.goose import List, Optional
from st3m.input import InputState, InputController
from st3m.ui.view import ViewManager
from st3m.application import Application, ApplicationContext
from ctx import Context
import bl00mbox
import leds
import math, cmath, random


def relative_altitude(pascal, celsius):
    # https://en.wikipedia.org/wiki/Hypsometric_equation
    return (celsius + 273.15) * (287 / 9.81) * math.log(101325 / pascal, math.e)

class Aurora:
    def __init__(self, color):
        self.generate_random()
        self.generate_phases()
        self.color = color
        self.timer = -1

    def generate_random(self):
        direction = cmath.exp(1j * math.tau * random.random())
        point = -120 * direction
        drift = 1
        points = [point]
        for _ in range(70):
            random_dir = cmath.exp(0.5j * (random.random() - 0.5))
            drift = random_dir
            # drift *= random_dir
            direction *= drift
            point += direction * (random.random() ** 2) * 15
            points.append(point)
        self.points = points

    def generate_phases(self):
        self.edge_phases = [cmath.phase(p) for p in self.points if abs(p) > 100]

class AuroraApp(Application):
    def get_help(self):
        return ("Move up and down to change the pitch!\n\n"
                "Press the app button to toggle whether you want the sound to keep "
                "playing when you exit.\n\n"
                "Under the hood this is a shepard tone generator hooked up to the "
                "barometer to modulate the pitch, plus a good amount of reverb.\n\n"
                "A shepard tone is a barber-pole like effect where sines of constant "
                "distance (typically an octave) can be shifted in one direction "
                'continuously by allowing notes to "overflow" and subtly tapering '
                "their volume around the overflow point.")

    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.blm = None
        self.alt = None
        self.alt_slow = None
        self.leds = [list() for _ in range(40)]
        self.num_oct = 7
        self.step_size = 12
        self.aurorae = [Aurora(col) for col in [(0,1,0.8,0.1), (0.7,0,1,0.1)]]
        self.background = False
        self.draw_background = 0

    def draw(self, ctx: Context) -> None:
        ctx.rgba(0, 0, 0, 0.2).rectangle(-120, -120, 240, 240).fill()
        layer_width = [2, 6, 20, 40, 100]
        ctx.line_width = 5

        ctx.translate(0, 50)
        for aurora in self.aurorae:
            if aurora.timer <= 0:
                aurora.generate_random()
                aurora.timer = 2000 + random.random() * 15000
                self.step_size = int(random.random() * 11) + 4
            else:
                aurora.generate_phases()

            ctx.rgba(*aurora.color)
            for point in aurora.points:
                for x, layer in enumerate(layer_width):
                    ctx.move_to(point.real, point.imag)
                    if layer > 20:
                        layer *= 1 + random.random() / 2
                    ctx.rel_line_to(0, -layer)
                    ctx.stroke()
        if self.draw_background:
            ctx.move_to(0,0)
            ctx.rgb(1,1,0)
            ctx.text_align = ctx.CENTER
            ctx.font_size = 20
            if self.background:
                text = "background on"
            else:
                text = "background off"
            ctx.text(text)
            self.draw_background -= 1

    def _build_synth(self):
        if self.blm is None:
            self.blm = bl00mbox.Channel("aurora borealis")
        self.oscs = [None] * self.num_oct
        self.mixer = self.blm.new(bl00mbox.plugins.mixer, self.num_oct)
        for i in range(self.num_oct):
            self.oscs[i] = self.blm.new(bl00mbox.plugins.osc)
            self.oscs[i].signals.waveform.switch.SINE = True
            self.oscs[i].signals.output >> self.mixer.signals.input[i]

        self.filter = self.blm.new(bl00mbox.plugins.filter)
        self.filter.signals.cutoff.freq = 3000
        self.mixer.signals.output >> self.filter.signals.input

        num_delays = 4
        delay_times = [300 * (1.618**x) for x in range(num_delays)]
        self.delays = [
            self.blm.new(bl00mbox.plugins.delay_static, delay_times[x])
            for x in range(num_delays)
        ]
        for x in range(num_delays - 1):
            self.delays[x].signals.output >> self.delays[x + 1].signals.input
        for x, delay in enumerate(self.delays):
            delay.signals.time = delay_times[x]
            delay.signals.dry_vol = 16000
            # self.delays[x].signals.feedback = 28000
        self.delays[0].signals.input << self.filter.signals.output

        self.delays[3].signals.dry_vol = 0
        self.delays[3].signals.level = 0
        self.blm.gain_dB = 0
        self.blm.signals.line_out << self.delays[num_delays - 1].signals.output
        self.fade_in = -0.1
        self.alt = None

        def background_think(ins, delta_ms):
            if self.fade_in < 1:
                self.fade_in += delta_ms / 10000
                self.fade_in = min(self.fade_in, 1)
                if self.fade_in > 0:
                    vol = self.fade_in * 16000
                    self.delays[num_delays-1].signals.dry_vol = vol
                    self.delays[num_delays-1].signals.level = vol
            sens = 2
            limit = 1/6000 #1/6000
            dampen = 0.2 #0.01
            # sens = 5
            if self.blm is None:
                return
            alt = relative_altitude(ins.imu.pressure, 15)
            if self.alt is None:
                self.alt = alt
                self.alt_slow = alt
            else:
                self.alt += (alt - self.alt) * dampen
                slow_limit = delta_ms * limit / sens
                self.alt_slow += max(min(self.alt - self.alt_slow, slow_limit), -slow_limit)

            blend = 2
            for i in range(self.num_oct):
                pitch = (sens * self.alt_slow + i) % self.num_oct
                vol = 1
                if pitch < blend:
                    vol = pitch / blend
                elif pitch > (self.num_oct - blend):
                    vol = (self.num_oct - pitch) / blend
                self.oscs[i].signals.pitch.tone = self.step_size * (pitch - 3)
                self.mixer.signals.input_gain[i].mult = vol
        self.blm.callback = background_think

    def on_enter(self, vm: Optional[ViewManager]) -> None:
        super().on_enter(vm)
        leds.set_slew_rate(90)
        if self.blm is not None:
            try:
                self.blm.foreground = True
            except bl00mbox.ReferenceError:
                self.blm = None
        if self.blm is None:
            self._build_synth()

    def on_exit(self):
        super().on_exit()
        if self.background:
            self.blm.background_mute_override = True
        else:
            self.blm.delete()
            self.blm = None

    def update_leds(self):
        if self.fade_in < 0:
            return
        #complete overkill but we had fun :3
        for x in range(len(self.leds)):
            self.leds[x] = list()
            
        for aurora in self.aurorae:
            for phase in aurora.edge_phases:
                xf = phase / math.tau * 40 + 10 + 2 * random.random()-1 % 40
                xi = int(xf)
                a1 = xf-xi
                a0 = 1-a1
                self.leds[xi % 40].append([c * a0 for c in aurora.color])
                self.leds[(xi + 1) % 40].append([c * a1 for c in aurora.color])

        for x in range(len(self.leds)):
            if self.leds[x]:
                color = [0,0,0]
                for col in self.leds[x]:
                    color[0] += col[0]
                    color[1] += col[1]
                    color[2] += col[2]
                div = max(5, * color)
                color = [c*self.fade_in/div for c in color]
            else:
                color = (0,0,0)
            leds.set_rgb(x, *color)
        leds.update()

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        if self.input.buttons.app.middle.pressed:
            self.background = not self.background
            self.draw_background = 5
        self.update_leds()
        for aurora in self.aurorae:
            if aurora.timer > 0:
                aurora.timer -= delta_ms



# For running with `mpremote run`:
if __name__ == "__main__":
    import st3m.run
    st3m.run.run_app(AuroraApp)
